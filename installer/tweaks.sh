#!/bin/sh

# disable watchdog and allow write access
echo 1 > /sys/class/gpio/Watchdog\ Disable/value
mount -o rw,remount /
/jci/bin/jci-log.sh stop

MYDIR=$(dirname "$(readlink -f "$0")")
. ${MYDIR}/utils.sh

CMU_SW_VER=$(get_cmu_sw_version)
rm -f "${MYDIR}/AIO_log.txt"

/jci/tools/jci-dialog --3-button-dialog --title="Tweaks Selection for Enable OPKG" --text="Choosing Method" --ok-label="Install" --cancel-label="Uninstall" --button3-label="Skip"
choice=$?

if [ "$choice" = '0' ]; then
 msg="Install.."
 check_compat
 show_message "INSTALLATION STARTED"

 if [ ! -e /etc/opkg/opkg.conf ]
 then
  log_message "Prepare opkg environment"
  mount -o rw,remount /
  rm -f /usr/lib/opkg
  rm -rf /tmp/mnt/data_persist/dev/opkg
  sleep 1
  mkdir -p /tmp/mnt/data_persist/dev/opkg
  ln -sf /tmp/mnt/data_persist/dev/opkg /usr/lib/opkg
  mkdir /etc/opkg
  cp ${MYDIR}/config/opkg/opkg.conf /etc/opkg

  log_message "Install autorun AND Upgrade wget"
  show_message "Install autorun"
  log_message "`opkg install ${MYDIR}/config/wget/autorun_1.2_arm.ipk`"
  show_message "Install autorun-plugins-checkonline"
  log_message "`opkg install ${MYDIR}/config/wget/autorun-plugins-checkonline_1.2_arm.ipk`"
  show_message "Install libuuid"
  log_message "`opkg install ${MYDIR}/config/wget/libuuid_1.0.3_arm.ipk`"
  show_message "Install libressl"
  log_message "`opkg install ${MYDIR}/config/wget/libressl_2.5.4_arm.ipk`"
  show_message "Install wget"
  log_message "`opkg install ${MYDIR}/config/wget/wget_1.19-2_arm.ipk`"
  show_message "Install libuv"
  log_message "`opkg install ${MYDIR}/config/wget/libuv_1.13.1_arm.ipk`"
  show_message "Install node"
  log_message "`opkg install ${MYDIR}/config/wget/node_0.12.18_arm.ipk`"
  show_message "Install websocketd"
  log_message "`opkg install ${MYDIR}/config/wget/websocketd_0.2.12-1_arm.ipk`"
  show_message "Install mzd-disable-watchdog"
  log_message "`opkg install ${MYDIR}/config/wget/mzd-disable-watchdog_1.0_arm.ipk`"
  show_message "Install mzd-apps"
  log_message "`opkg install ${MYDIR}/config/wget/mzd-apps_0.1-1_arm.ipk`"
  show_message "Install mzd-aiotweaks"
  log_message "`opkg install ${MYDIR}/config/wget/mzd-aiotweaks_1.0-8_arm.ipk`"
  show_message "Install finished"
  show_message "REBOOT .."
  killall jci-dialog
  reboot

 else
  log_message "Already installed"
  show_message "Already Installed"
 fi
elif [ "$choice" = '1' ]; then
  msg="Uninstall.."
  mount -o rw,remount /
  mount -o rw,remount /tmp/mnt/resources
  if [ -e /etc/opkg/opkg.conf ]; then
   log_message "Kill process"
   log_message "pkill -f start"
   pkill -f start
   log_message "pkill -f headunit"
   pkill -f headunit
   log_message "pkill -f websocketd"
   pkill -f websocketd
   log_message "pkill -f olsrd"
   pkill -f olsrd
   log_message "`opkg remove --force-removal-of-dependent-packages libuv`"
   log_message "`opkg remove --force-removal-of-dependent-packages libuuid`"
   log_message "`opkg remove --force-removal-of-dependent-packages autorun`"
   
   mount -o rw,remount /
   mount -o rw,remount /tmp/mnt/resources
   
   rm -rf /tmp/mnt/resources/aio
   rm -rf /tmp/mnt/data_persist/dev/opkg
   rm -f /usr/lib/opkg
   rm -rf /etc/opkg
   
  else 
   show_message "Not found OPKG. Uninstall abort"
   log_message "Remove old files"
   log_message "`rm -rf /tmp/mnt/resources/aio`"
   log_message "`rm -rf /tmp/mnt/data_persist/dev/opkg`"
   log_message "`rm -f /usr/lib/opkg`"
   log_message "`rm -rf /etc/opkg`"
  fi
  killall jci-dialog
  show_message "Remove finished"
  show_message "REBOOT .."
  reboot
else
 msg="Abort.."
 show_message "INSTALLATION ABORTED PLEASE UNPLUG USB DRIVE"
fi

