#!/bin/sh

ENABLE=0

[ $ENABLE -eq '0' ] && exit

VSS=/tmp/root/.vss

if ! [ -e ${VSS}/Drivetrain.json ]; then
 jq < ${VSS}/cars.json '.Attribute.children.Drivetrain.children' > ${VSS}/Drivetrain.json
fi

jq < ${VSS}/Drivetrain.json '.Transmission.children.DriveType.value="forward wheel drive"' |sponge ${VSS}/Drivetrain.json
jq < ${VSS}/Drivetrain.json '.Transmission.children.Type.value="automatic"'                |sponge ${VSS}/Drivetrain.json
jq < ${VSS}/Drivetrain.json '.Transmission.children.GearCount.value=6'                     |sponge ${VSS}/Drivetrain.json

jq < ${VSS}/Drivetrain.json '.InternalCombustionEngine.children.FuelType.value="diesel"'   |sponge ${VSS}/Drivetrain.json
jq < ${VSS}/Drivetrain.json '.InternalCombustionEngine.children.MaxTorque.value=250'       |sponge ${VSS}/Drivetrain.json
jq < ${VSS}/Drivetrain.json '.InternalCombustionEngine.children.Displacement.value=1500'   |sponge ${VSS}/Drivetrain.json
jq < ${VSS}/Drivetrain.json '.InternalCombustionEngine.children.MaxPower.value=105'        |sponge ${VSS}/Drivetrain.json

jq < ${VSS}/Drivetrain.json '.FuelSystem.children.HybridType.value="not_applicable"'       |sponge ${VSS}/Drivetrain.json
jq < ${VSS}/Drivetrain.json '.FuelSystem.children.TankCapacity.value=44'                   |sponge ${VSS}/Drivetrain.json
jq < ${VSS}/Drivetrain.json '.FuelSystem.children.FuelType.value="diesel"'                 |sponge ${VSS}/Drivetrain.json
