var turnScreenOff = false;
var turnWifiOn = false; // this is experimental and may not work yet
var AIOlonghold = false;

function applyTweaks() {
  var head = document.querySelector("head");
  var body = document.getElementsByTagName("body")[0];
  if (!window.jQuery) {
    utility.loadScript("addon-common/jquery.min.js");
  }
  var tweaks = localStorage.getItem("aio.tweaks") || "";
  if (tweaks.length > 0) {
    var AIOcss = document.createElement("link");
    AIOcss.href = "apps/_aiotweaks/css/_aiotweaksApp.css";
    AIOcss.rel = "stylesheet";
    AIOcss.type = "text/css";
    body.insertBefore(AIOcss, body.firstChild);
    body.className = tweaks;
  }
}

framework.transitionsObj._genObj._TEMPLATE_CATEGORIES_TABLE.AIOTweaksTmplt = "Detail with UMP";

applyTweaks();
/* ** Attempt to start speedometer app on boot **
** Works in the emulator but not in the car ** * /
setTimeout(function(){
framework.sendEventToMmui("system","SelectApplications");
setTimeout(function(){aioMagicRoute("_speedometer","Start");}, 4000);
}, 30000);
*/
utility.loadScript('apps/_aiotweaks/js/mzd.js', null, function() {
  if (turnWifiOn) {
    framework.common._contextCategory._contextCategoryTable['netmgmt.*'] = "Other";
    setTimeout(function() {
      turnOnWifi();
    }, 3000); // 3 second delay to let everything load up
  }
  if (turnScreenOff) {
    setTimeout(function() {
      turnScreenOff = setInterval(function() {
        if (framework.getCurrCtxtId() === "DisplayOff") {
          clearInterval(turnScreenOff);
          turnScreenOff = null;
        } else {
          framework.sendEventToMmui("common", "Global.IntentSettingsTab", { payload: { settingsTab: "Display" } });
          framework.sendEventToMmui("common", "Global.IntentHome");
        }
        setTimeout(function() {
          framework.getCurrCtxtId() === "DisplayOff" ? null : framework.sendEventToMmui("syssettings", "SelectDisplayOff");
        }, 1000);
      }, 5000);
    }, 5000);
  }
});

function StartAIOApp() {
  try {
    $('#SbSpeedo, #Sbfuel-bar-wrapper').fadeOut();
    //framework.sendEventToMmui("common", "SelectBTAudio");
  } catch (err) {

  }
  // *****************************
  // ** Setting of Buttons BEGIN!
  // *****************************
  // AIO info
  getAppListData();
  $('button').on('click', function() {
    $('button').removeClass('selectedItem');
    $(this).addClass('selectedItem')
  });
  $("#aioInfo").on("click", function() { showAioInfo("<div class='infoMessage'><h1>AIO Tweaks App v" + aioTweaksVer + " </h1>This is an experimental app by Trezdog44 made to test the capabilities, functionalities, and limitations of apps in the MZD Infotainment System.<br>This app has some useful and fun functions although it is not guaranteed that everything works.  There may be non-functioning or experimental features.</div>"); });
  $("#aioReboot").on("click", myRebootSystem);
  //$("#mainMenuLoop").on("click",setMainMenuLoop);
  $("#test").on("click", myTest);
  $("#touchscreenBtn").on("click", enableTouchscreen);
  $("#touchscreenOffBtn").on("click", disableTouchscreen);
  $("#touchscreenCompassBtn").on("click", enableCompass);
  $("#adbBtn").on("click", adbDevices);
  $("#adbRevBtn").on("click", adbReverse);
  $("#messageTestBtn").on("click", messageTest);
  //$("#screenshotBtn").on("click",takeScreenshot);
  //$("#saveScreenshotBtn").on("click",saveScreenshot);
  $("#AAstart").on("click", startHeadunit);
  $("#AAstop").on("click", stopHeadunit);
  $("#CSstart").on("click", startCastScreen);
  $("#CSstop").on("click", stopCastScreen);
  //$("#SPstart").on("click",startSpeedometer);
  //$("#SPstop").on("click",stopSpeedometer);
  //$("#chooseBg").on("click",chooseBackground);
  $("#systemTab").on("click", settingsSystemTab);
  $("#wifiSettings").on("click", wifiSettings);
  //$("#runTweaksBtn").on("click",playAllVideos);
  $("#fullRestoreConfirmBtn").on("click", fullSystemRestoreConfirm);
  $("#headunitLogBtn").on("click", showHeadunitLog);
  $("#scrollUpBtn").on("click", scrollUp);
  $("#scrollDownBtn").on("click", scrollDown);
  $("#appListBtn").on("click", showAppList);
  $("#showEnvBtn").on("click", showEnvVar);
  $("#closeAioInfo").on("click", closeAioInfo);
  $("#showDFHBtn").on("click", showDFH);
  $("#showPSBtn").on("click", showPS);
  $("#showMeminfoBtn").on("click", showMeminfo);
  $("#toggleWifiAPBtn").on("click", toggleWifiAP);
  $("#stopFirewallBtn").on("click", stopFirewall);
  $("#displayOffBtn").on("click", displayOff);
  $("#mountSwapBtn").on("click", mountSwap);
  $("#unmountSwapBtn").on("click", unmountSwap);
  $("#showVehData").on("click", showVehicleData);
  $("#backupCamBtn").on("click", backUpCam);
  $("#errLogBtn").on("click", showErrLog);
  $("#runTerminalBtn").on("click", TerminalConfirm);
  $("#runCheckIPBtn").on("click", RunCheckIP);
  $("#reverseAppListBtn").on("click", reverseApplicationList);
  $("#shiftEntListBtn").on("click", shiftEntertainmentList);
  $("#devModeSecretBtn").on("click", toggleDevMode);
  $("#wifiToggle").on("click", turnOnWifi);
  $("#verBtn").on("click", showVersion);
  $("#showBgBtn").on("click", function() { $("html").addClass("showBg") });
  $("#twkOut").on("click", function() { framework.sendEventToMmui("common", "Global.IntentHome") });
  $("#usba").on("click", function() { framework.sendEventToMmui("system", "SelectUSBA") });
  $("#usbb").on("click", function() { framework.sendEventToMmui("system", "SelectUSBB") });
  $("#pauseBtn").on("click", function() { localStorage.clear(); });
  //$("#previousTrackBtn").on("click",function(){framework.sendEventToMmui("common", "Global.PreviousHoldStop")});
  //$("#nextTrackBtn").on("click",function(){framework.sendEventToMmui("common", "Global.NextHoldStop")});
  $("#BluetoothAudio").on("click", function() { framework.sendEventToMmui("system", "SelectBTAudio") });
  $("#previousTrackBtn").on("click", function() { framework.sendEventToMmui("Common", "Global.Previous") });
  $("#nextTrackBtn").on("click", function() { framework.sendEventToMmui("Common", "Global.Next") });
  $(".mmLayout").on("click", function() {
    changeLayout($(this).attr("id"));
    $("#MainMenuMsg").html($(this).text());
  });
  $(".toggleTweaks").on("click", function() {
    $("body").toggleClass($(this).attr("id"));
    $("#MainMenuMsg").html($(this).text());
  });
  $("#clearTweaksBtn").on("click", function() {
    $("body").attr("class", "");
    $("#MainMenuMsg").text("Main Menu Restored");
    localStorage.removeItem("aio.tweaks");
  });
  //$("#touchscreenToggle").on("click",toggleTSPanel);
  $("#closeTouchPanel").on("click", closeTSPanel);
  // Tab select & localStrage save on each button press
  $(".toggleTweaks").on("click", saveTweaks);
  $(".tablinks").on("click", function() {
    $("#MainMenuMsg").html("");
    localStorage.setItem("aio.prevtab",$(this).attr("tabindex"));
  });
  $("#openNav").on("click", function() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  });
  $("#closeNav").on("click", function() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
  });
  /*if (typeof(Storage) !== "undefined") {
    console.log("localStorage Supported: " + JSON.stringify(localStorage));
  } else {
    console.log("localStorage Not Supported!!");
  }*/
}

