# Command build
* borrow from OpenWRT

## Howto build ipk

```bash
mkdir ca-certificates
cd ca-certificates
wget https://downloads.openwrt.org/chaos_calmer/15.05.1/imx6/generic/packages/base/ca-certificates_20150426_imx6.ipk
tar xf ca-certificates_20150426_imx6.ipk
mkdir CONTROL
cd CONTROL
tar xf ../control.tar.gz
sed -i 's/Depends: libc/Depends: /g' control
sed -i 's/Architecture: imx6/Architecture: arm/g' control
sed -i 's/package\/system\/ca-certificates/packages\/utils/g' control
rm -f  postinst  prerm

sed -i '/Depends:/a \
Priority: standard\
Maintainer: khantaena <khantaena@gmail.com>' control

cd ..
rm -f control.tar.gz
tar xf data.tar.gz
rm -f data.tar.gz
rm -f debian-binary
rm -f ca-certificates_20150426_imx6.ipk

mv etc data_persist
cd ..

opkg-build ca-certificates
```
